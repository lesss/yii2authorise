<?php

use yii\db\Migration;

class m170621_160247_userfittch extends Migration
{
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%user}}', [
            'id' => $this->primaryKey(),
            'password_hash' => $this->string()->notNull(),
            'email' => $this->string()->notNull()->unique(),
            'status' => $this->smallInteger()->notNull()->defaultValue(10),
            'created_at' => $this->integer()->notNull(),
            ], $tableOptions);

        $this->createTable('{{%userSocialNetwork}}', [
            'id' => $this->primaryKey(),
            'user_id' => $this->integer()->notNull(),
            'social_network_id' => $this->integer()->notNull(),
            'user_token' => $this->string()->unique(),
            ], $tableOptions);
    }

    public function safeDown()
    {
        echo "m170621_160247_userfittch cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m170621_160247_userfittch cannot be reverted.\n";

        return false;
    }
    */
}
