<?php

/*
 * @property integer $id
 * @property string $email
 * @property string $password
 * @property integer $created_at
 * @property integer $status
 */

class User extends \yii\db\ActiveRecord
{

    public static function tableName()
    {
        return 'user';
    }


    public function rules()
    {
        return [
            [['email'],'required'],
            [['email','password'],'string','max' => 30],
        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'email' => 'Email',
            'password' => 'Password',
        ];
    }
}
